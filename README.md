# Antifa

This app shows the Antifa logo on the CCCamp23 flow3r badge screen and sets all LEDs to red.

The image was taken from Wikimedia Commons: https://commons.wikimedia.org/wiki/File:Antifalogo.svg
